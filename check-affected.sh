#!/bin/bash
set -e

if ! grep -q $1 "affected.txt"; then
  echo "Nothing to do here";
  mkdir -p "test-reports/${1}";
  cp -f "empty-junit.xml" "test-reports/${1}/junit.xml";

  exit 0;
fi
